"""
Draw a closed polygon with a number of vertices passed as a command line parameter
"""

import cv2
import numpy as np
import math
import argparse


# Returns a list of points that corresponds to the polygon boundary vertices.
def calculatePolyVertices(centerx, centery, radius, numvertices):
    vertexPoints = []

    for i in range(0, 360, int(360/numvertices)):
        newx = int(math.cos(math.pi * i / 180) * radius)
        newy = int(math.sin(math.pi * i / 180) * radius)
        point = np.array([centerx + newx, centery + newy])
        vertexPoints.append(point)

    return np.array(vertexPoints)


# Validates command line parameters
def validateParams():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--points", required=True, help="Number of points of the shape", type=int)
    parser.add_argument("-r", "--radius", required=True, help="Radius of the shape", type=int)

    return parser.parse_args()


if __name__ == '__main__':

    # Get params from command line
    args = validateParams()
    numberpoints = args.points
    radius = args.radius

    # Set a default value if invalid number of points
    if numberpoints < 3 or numberpoints > 360:
        print("Invalid number of points. Default value set to 12")
        numberpoints = 12

    # Set a default radius if radius is very small
    if radius < 20:
        print("Invalid radius. Default value set to 100")
        radius = 100

    # Create canvas
    canvas = np.zeros((800, 1200, 3), dtype="uint8")

    # Get vertices according to number of points set by user
    pts = calculatePolyVertices(int(canvas.shape[1]/2), int(canvas.shape[0]/2), radius, numberpoints)

    # Reshape to shape (number_vertex, 1, 2)
    pts = pts.reshape((-1, 1, 2))

    # Render the polygon
    cv2.polylines(canvas, [pts], True, (255, 0, 0), 3, cv2.LINE_AA)

    cv2.imshow("Polygon", canvas)
    cv2.waitKey()
    cv2.destroyAllWindows()
